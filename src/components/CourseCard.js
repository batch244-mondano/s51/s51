import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

	// Decontruct the course properties into their own variables
	const { name, description, price} = courseProp;

	// Syntax:
	// const [getter, setter] = useState(initialGetterValue)
	const [count, setCount] = useState(0);

	// useState hook for getting and setting the seats for the course
	const [ seats, setSeats] = useState(30);

	const [ isOpen, setIsOpen] = useState(false);


	// Function that keeps track of the enrollees for a course
	function enroll(){
		// if (seats >= 0){
		setSeats(seats - 1);
		setCount(count + 1);
		console.log(`Enrollees: ${count}`);	
		// } else {
			// return alert("No more seats available.");
		// }
	}

	useEffect(() => {
		if(seats === 0) {
			setIsOpen(true);
		}
	}, [seats]);

    return (
	    <Card>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>Php {price}</Card.Text>
	            <Card.Text>Seats: {seats}</Card.Text>
	            <Button variant="primary" onClick={enroll} disabled={isOpen}>Enroll</Button>
	        </Card.Body>
	    </Card>  

    )
}