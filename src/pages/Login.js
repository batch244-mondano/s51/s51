import { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Login(){

	// Allows us to consume the User context object and its properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [isActive, setIsActive] = useState(true);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);

	function loginUser(e) {

		e.preventDefault();

		localStorage.setItem('email', email);

		// Set the global user state to have properties obtained from local storage
		// Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
		// When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering

		setUser({email:localStorage.getItem('email')});

		setEmail('');
		setPassword1('');

		alert('You are now logged in!');
	}

	useEffect(() => {
		if((email !== '' && password1 !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1]);


	return(

		// Conditional rendering that ill redirect the user to the course page
		(user.email !== null)
		?
			<Navigate to='/courses'/>
		:
		// Invokes the authentication function upon clicking on submit button
		<Form onSubmit={(e) => loginUser(e)}>
			<Form.Group controlId = "userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
				<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
			</Form.Group>


			{isActive
				?
					<Button variant="primary" type="submit" id="submitBtn">Log-in</Button>
				:
					<Button variant="danger" type="submit" id="submitBtn" disabled>Log-in</Button>
			}
			
		</Form>
	)
}