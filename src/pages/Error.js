/*import { Link } from 'react-router-dom';
import { div } from 'react-bootstrap'

export default function Error() {

     localStorage.clear()

    return (
        <>
        <div className='content'>
            <h1>Page Not Found!</h1>
            <h4>Go back na sa <Link to="/">homepage</Link></h4>
        </div>
        </>
    )    
}*/

import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title : "404 - Not Found",
        content : "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back home"
    }

    return (
        <Banner data={data}/>
    )
}